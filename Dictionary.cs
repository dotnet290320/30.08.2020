using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _30aug20
{
    class Program
    {

       class Person
        {
            public int ID { get; set; }
            public string Name { get; set; }

            public override int GetHashCode()
            {
                return ID;
            }

            public override string ToString()
            {
                return $"{base.ToString()} ID: {ID} Name : {Name}";
            }
        }
        class Car
        {
            public string Name { get; set; }
            public int Year { get; set; }
        }
        static void Main(string[] args)
        {
            Person p1 = new Person { ID = 20, Name = "A" }; // 20
            Person p2 = new Person { ID = 20, Name = "A" }; // database -- xml serializer // 20
            // p1 == p2 // operator overloading

            // key   : int[id]
            // value : person

            Console.WriteLine(p1.GetHashCode());
            Console.WriteLine(p2.GetHashCode());

            Console.WriteLine(p1.GetHashCode());
            Console.WriteLine((3.14).GetHashCode());
            List<Person> people = new List<Person>()
            {
                new Person { ID = 1, Name = "Danny"},
                new Person { ID = 2, Name = "Nemo"},
                new Person { ID = 3, Name = "Moshe"}
            };

            Dictionary<int, Person> mapIdToPerson2 = new Dictionary<int, Person>();
            people.ForEach(onePerson => mapIdToPerson2[onePerson.ID] = onePerson);
            foreach (var item in people)
            {

            }
            var x = 12.4f;
            var y = 4;
            float z = x + y;
            //foreach (Person onePerson in people)
            //{
            //    mapIdToPerson2[onePerson.ID] = onePerson;
            //}

            Person person_with_id_3 = mapIdToPerson2[3];

            //people.ForEach(item => map)

            Console.WriteLine(people.GetHashCode());

            foreach (Person p in people)
            {
                if (p.ID == 2)
                {
                    Console.WriteLine("We found nemo!");
                }
            }
            // Dictionary / Hashmap
            // Key -- > value
            // key could be (almost) anything --> value could be (alsmot) anything
            // "One" : the first number in decimal
            // "Two" : the second number in decimal
            // "Banana" : a tropical fruit

            // key [20]   : value
            // unique :  can be duplicate
            // int : string
            // 55 : "hello" -- delete and replaced
            // 55 : "goodbye"
            // 77 : "goodbye"
            // f(3) -- > 19
            // f(4) -- > 19
            // f(3) -- > 21 NO

            // -- object
            // hash collision
            // algo: take key / 10 -- key 
            // 87
            // key : 8 -> value : 87
            // 96
            // key : 9 -> value : 96
            // 32
            // key : 3 -> value : 32 [hash-code:32]
            // 34 [hash collision]
            // key : 3 -> value : 32 [hash-code:32] -> 34 [hash-code:34]

            Console.WriteLine("=======================================");
            Dictionary<int, Person> mapIdToPerson = new Dictionary<int, Person>();
            /*
                  List<Person> people = new List<Person>()
            {
                new Person { ID = 1, Name = "Danny"},
                new Person { ID = 2, Name = "Nemo"},
                new Person { ID = 3, Name = "Moshe"}
            };
            */
            Person danny = new Person { ID = 1, Name = "Danny" };
            mapIdToPerson.Add(1, danny); // will add, if exist -- will explode!
            //mapIdToPerson.Add(1, danny); // explode!

            Person nemo = new Person { ID = 2, Name = "Nemo" };
            mapIdToPerson[2] = nemo; // will add or overwrite
            mapIdToPerson[1] = danny;

            Person person1 = mapIdToPerson[2]; // unsafe , if key not exist -- will explode!
            // Person person2 = mapIdToPerson[3];

            // safe way to search in dictionary
            Person another_person_with_id_3;
            if (mapIdToPerson.TryGetValue(key:3, value: out another_person_with_id_3))
            {
                // person with id 3 was found
                // another_person_with_id_3 will contain the object
            }
            else
            {
                // person with id 3 was not foundf
                // another_person_with_id_3 will be null
            }

            // safe way to search in dictionary
            //Person another_person_with_id_3;
            if (mapIdToPerson.TryGetValue(key: 3, value: out Person another_person_with_id_33))
            {
                // person with id 3 was found
                // another_person_with_id_3 will contain the object
            }
            else
            {
                // person with id 3 was not foundf
                // another_person_with_id_3 will be null
            }

            if (mapIdToPerson.ContainsKey(4))
            {
                // person with id 4 was found
                Person person4 = mapIdToPerson[4];
            }
            else
            {
                // person with id 4 was not found
            }

            foreach (KeyValuePair<int, Person> item in mapIdToPerson)
            {
                Console.WriteLine($"KEY : {item.Key} VALUE : {item.Value}");
            }

        }
    }
}
